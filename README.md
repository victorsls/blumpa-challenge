# Blumpa Challenge
Esta aplicação é um desafio para a vaga de desenvolvedor backend na Blumpa

## Requisitos
- [rbenv](https://github.com/rbenv/rbenv)
- Ruby 2.7.0
- Rails 6.0.2.1

## Clonar o repositório
```
git clone https://bitbucket.org/victorsls/blumpa-challenge.git
```

## Instalar as depêndencias
``` 
bundle install
```

## Variáveis de ambiente
No arquivo **credentials.yml.enc** temos a variável **rick_and_morty_api_url**, este arquivo só pode ser utilizado 
quando possuímos a chave mestre. Ela não está no repositório por motivos de segurança.
Como este projeto é de demonstração podemos compartilhar a chave.

Para conseguir utilizar o arquivo precisamos executar o seguinte comando:
```
echo 4823d4b33c1c4856f0b92bcf7e66d01d > config/master.key
```

## Executar o servidor de desenvolvimento
```
rails s
```

## Executar os testes
```
rspec
```

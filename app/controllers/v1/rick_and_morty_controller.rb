class V1::RickAndMortyController < ApplicationController
  def first_air_date
    render get_first_episode(params['id'])
  end

  def get_first_episode(id)
    rick_and_morty_api_url = Rails.application.credentials.rick_and_morty_api_url
    response = HTTParty.get("#{rick_and_morty_api_url}/character/#{id}")
    if response.success?
      first_episode_url = JSON.parse(response.body)['episode'][0]
      response = HTTParty.get(first_episode_url)
      if response.success?
        first_air_date = DateTime.parse(JSON.parse(response.body)['air_date']).strftime('%d/%m/%Y')
        return {'json': {'first_air_date': first_air_date}, 'status': :ok}
      end
    end
    {'json': JSON.parse(response.body), 'status': response.code}
  end
end
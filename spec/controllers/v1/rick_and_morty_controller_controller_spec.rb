require 'rails_helper'

RSpec.describe V1::RickAndMortyController, type: :controller do
  context "success case" do
    describe "get the date of a character's first episode", :type => :request do
      before { get "/api/v1/first-air-date/#{rand(1..10)}" }

      it 'returns status code 200' do
        expect(response).to have_http_status(:success)
      end

      it 'checking body keys' do
        expect(JSON.parse(response.body).keys).to contain_exactly('first_air_date')
      end
    end
  end

  context "error case" do
    describe "get the date of a character's first episode", :type => :request do
      before { get "/api/v1/first-air-date/#{1000}" }

      it 'returns status code 404' do
        expect(response).to have_http_status(:not_found)
      end

      it 'checking body keys' do
        expect(JSON.parse(response.body).keys).to contain_exactly('error')
      end
    end
  end
end
